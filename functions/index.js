const functions = require('firebase-functions');
var firebase = require('firebase-admin');
var express = require('express');

var firebaseApp = firebase.initializeApp(
    functions.config().firebase
)
var app = express();

function getFacts() {
    var ref = firebase.database().ref('magic');
    return ref.on('value', (data) => {
        console.log("data", data.val());
    })
}

app.get('/test', function (req, res) {
    res.set('Cache-Control', 'public, max-age=300', 's-maxage=600')
    getFacts()
    // .then(snap => {
    //     console.log(snap);
    res.send('test working');
    // })
})

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

exports.app = functions.https.onRequest(app);
